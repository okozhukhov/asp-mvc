﻿using Marine_web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Marine_web.Controllers
{
    public class ReportsController : Controller
    {
        [Authorize]
        public IActionResult pdf()
        {

            ShipDataManager _shipData = new ShipDataManager();

            //get id and name from identity
            ViewData["name"] = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.Name).Value;
            string nid = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.NameIdentifier).Value;
            string sid = "0";
            if (Url.ActionContext.RouteData.Values["ship"] != null)
            {
                sid = GetNumbers(Url.ActionContext.RouteData.Values["ship"].ToString());
            }
            
            ViewData["ShipList"] = _shipData.GetShips(nid);
            ViewData["PdfList"] = _shipData.GetPdf(nid, sid);

            return View();
        }

        [Authorize]
        public IActionResult charts()
        {
            ShipDataManager _shipData = new ShipDataManager();

            //get id and name from identity
            ViewData["name"] = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.Name).Value;
            string nid = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.NameIdentifier).Value;
            string sid = "0";
            if (Url.ActionContext.RouteData.Values["ship"] != null)
            {
                sid = GetNumbers(Url.ActionContext.RouteData.Values["ship"].ToString());

                ViewData["ShipList"] = _shipData.GetShips(nid);
                ViewData["ChartData"] = _shipData.GetPdf(nid, sid);

                return View();
            }
            else
            {
                return Redirect("/");
            }

        }

        //Used to clear id in url sometimes extra characters are included that breaks the API call
        private static string GetNumbers(string input)
        {
            return new string(input.Where(c => char.IsDigit(c)).ToArray());
        }
    }
}
