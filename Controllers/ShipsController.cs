﻿using Marine_web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Claims;

namespace Marine_web.Controllers
{
    public class ShipsController : Controller
    {
        private readonly ILogger<ShipsController> _logger;
        ShipDataManager _shipData = new ShipDataManager();

        public ShipsController(ILogger<ShipsController> logger)
        {
            _logger = logger;
        }

        [Authorize]
        public IActionResult Index()
        {
            //get id and name from identity
            ViewData["name"] = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.Name).Value;
            string nid = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.NameIdentifier).Value;


            ViewData["ShipList"] = _shipData.GetShips(nid);
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
