﻿using Marine_web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Marine_web.Controllers
{
    public class AccountController : Controller
    {
        readonly UserManager _userManager;
        public AccountController(UserManager userManager)
        {
            _userManager = userManager;
        }
        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Logout()
        {
            SignOut();
            return RedirectToAction("Home", null);
        }

        [HttpPost]
        public IActionResult LogIn(LoginViewModel form)
        {
            if (!ModelState.IsValid)
                return View(form);
            try
            {
                _userManager.SignIn(this.HttpContext, form.userName, form.password);
                return RedirectToAction("Index", "Ships", null);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError("summary", ex.Message);
                return View(form);
            }
        }
    }
}
