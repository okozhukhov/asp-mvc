﻿using Marine_web.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;

namespace Marine_web
{
    public class ShipDataManager
    {

        public IList<ShipList> GetShips(string uid)
        {
            string furl = "http://update.oss.gr:56345/OPTIMUM/opsw/optimum/web/rest/customervessels?code=" + uid;
            var client = new RestClient(furl);
            var request = new RestRequest();
            var response = client.GetAsync(request).Result;
            client.Dispose();
            IList<ShipList> DeserialShipList = JsonConvert.DeserializeObject<IList<ShipList>>(response.Content);

            return DeserialShipList;
        }

        public IList<PdfModel> GetPdf(string uid, string nid = "0", string edate = "31-12-2121", string sdate = "1-1-2021")
        {
            string furl = "http://update.oss.gr:56345/OPTIMUM/opsw/optimum/web/rest/getpdf01list?code=" + uid + "&vesselid="+ nid +"&datefrom="+ sdate +"&dateto="+ edate;
            var client = new RestClient(furl);
            var request = new RestRequest();
            var response = client.GetAsync(request).Result;
            client.Dispose();
            IList<PdfModel> DeserialPdfList = JsonConvert.DeserializeObject<IList<PdfModel>>(response.Content);

            return DeserialPdfList;
        }

        public void LoadPdf()
        {
            //todo pdf download
            string furl = "http://update.oss.gr:56345/OPTIMUM/opsw/optimum/web/rest/getpdf01?code=407&pdfcode=yHAWFhsDh8fuB5z2qd0I";


        }

        public IList<ChartModel> GetChart(string uid, string nid, string edate = "31-12-2121", string sdate = "1-1-2021")
        {
            string furl = "http://update.oss.gr:56345/OPTIMUM/opsw/optimum/web/rest/rep01?code=" + uid + "&vesselid=" + nid + "&datefrom=" + sdate + "&dateto=" + edate;
            var client = new RestClient(furl);
            var request = new RestRequest();
            var response = client.GetAsync(request).Result;
            client.Dispose();
            IList<ChartModel> DeserialChartList = JsonConvert.DeserializeObject<IList<ChartModel>>(response.Content);

            return DeserialChartList;
        }
    }
}
