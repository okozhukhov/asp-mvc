$(window).on("load", function(){
    if($("body.loginpage").length) {
        var introAnimation = gsap.timeline();

        introAnimation.to(".section-intro-counter", {
            top:'-100%',
            ease: "power2.inOut",
            duration: 2,
            delay: 1,
            onComplete: function() {
				if($('.bsc-mobile:visible').length) {
                    $(".section-intro-counter").fadeOut();
                }
            }
        }).to(".intro-bg-animation-image", {
            backgroundPosition: "50% 50%",
            ease: "power2.inOut",
            scale:1.2,
            duration: 2
        },"<").to(".section-intro-top, .section-intro-footer", {
            css:{opacity: 1},
            duration: 1.8
        },"-=1.5").to(".section-intro-main-top", {
            css: {
                opacity: 1.2,
                marginBottom: "50px"
            },
            duration: 1
        },"-=1.3").to(".box-login", {
            css:{opacity: 1},
            duration: 1.5
        },"-=1");
    }
});

$( function() {
    if($('.select-2').length) {
        $('.select-2').each(function() {
            var options = {
                minimumResultsForSearch: -1,
                width: "style"
            };

            if($(this).attr('data-select2-placeholder') !== undefined) {
                options.placeholder = $(this).attr('data-select2-placeholder');
            }

            $(this).select2(options);
        });
    }
    if($('.datepicker-container').length) {
        // https://jqueryui.com/datepicker/#date-range
        var dateFormat = "mm/dd/yy",
            from = $( "#date-from" )
            .datepicker({
                defaultDate: "+1w",
                changeMonth: true
            })
            .on( "change", function() {
                to.datepicker( "option", "minDate", getDate( this ) );
            }),
            to = $( "#date-to" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true
            })
            .on( "change", function() {
            from.datepicker( "option", "maxDate", getDate( this ) );
            });
        
        function getDate( element ) {
            var date;
            try {
            date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
            date = null;
            }
        
            return date;
        }
    }

    // https://css-tricks.com/snippets/jquery/image-preloader/
    $("[data-preload-image]").each(function() {
        $("<img />").attr("src", $( this ).data( "preloadImage" ));
    });
});