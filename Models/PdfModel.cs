﻿namespace Marine_web.Models
{
    public class PdfModel
    {

        public string id { get; set; }
        public string vesselid { get; set; }
        public string vesselname { get; set; }
        public string datefrom { get; set; }
        public string dateto { get; set; }
        public string pdfcode { get; set; }
    }
}
