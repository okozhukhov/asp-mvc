﻿namespace Marine_web.Models
{
    public class LoginViewModel
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
