﻿using System.Collections.Generic;

namespace Marine_web.Models
{
    public class Value
    {
        public string date { get; set; }
        public double value { get; set; }
    }

    public class Unitset2
    {
        public int id { get; set; }
        public string graphtitle { get; set; }
        public string vaxistitle { get; set; }
        public double lowerlim { get; set; }
        public double upperlim { get; set; }
        public double vaxismin { get; set; }
        public double vaxismax { get; set; }
        public List<Value> values { get; set; }
    }

    public class Dosageset
    {
        public int id { get; set; }
        public string description { get; set; }
        public string units { get; set; }
        public List<Value> values { get; set; }
    }

    public class Unitset
    {
        public int id { get; set; }
        public string description { get; set; }
        public List<Unitset> unitset { get; set; }
        public List<Dosageset> dosageset { get; set; }
    }

    public class System
    {
        public int id { get; set; }
        public string description { get; set; }
        public List<Unitset> unitsets { get; set; }
    }

    public class Rep
    {
        public bool datafound { get; set; }
        public int vesselid { get; set; }
        public List<System> systems { get; set; }
    }


    public class ChartModel
    {
        public Rep rep { get; set; }
    }
}
