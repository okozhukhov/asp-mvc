﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marine_web.Models
{
    public class ShipList
    {
        public string id { get; set; }
        public string name { get; set; }
        public string imo { get; set; }
    }
}
