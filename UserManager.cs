﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Marine_web
{
    public class UserManager
{

    public UserManager(string connectionString)
    {
        
    }

    public async void SignIn(HttpContext httpContext, string user, string pass, bool isPersistent = false)
    {
            //API string
            string furl = "http://update.oss.gr:56345/OPTIMUM/opsw/optimum/web/rest/login?username=" + user + "&password=" + pass;
            //RESTSHARP call
            var client = new RestClient(furl);
            var request = new RestRequest();
            var content = "";
            string id = "0";
            string name = "null";

            try
            {
                //get response from API and set id and name
                var response = client.GetAsync(request).Result;
                content = response.Content;
                id = JObject.Parse(content)["code"].ToString();
                name = JObject.Parse(content)["name"].ToString();
            }
            catch (Exception ex)
            {
                //Set content to wrong credentials if there is an issue
                content = "Wrong credentials";
            }



            //recheck for credential response
            if (content != "Wrong credentials")
            {
                //set identity claim and cookie
                ClaimsIdentity identity = new ClaimsIdentity(this.GetUserClaims(id, name), CookieAuthenticationDefaults.AuthenticationScheme);
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                
            }
        }

    public async void SignOut(HttpContext httpContext)
    {
        await httpContext.SignOutAsync();
    }

    private IEnumerable<Claim> GetUserClaims(string id, string name)
    {
        List<Claim> claims = new List<Claim>();

        claims.Add(new Claim(ClaimTypes.NameIdentifier, id));
        claims.Add(new Claim(ClaimTypes.Name, name));
        claims.AddRange(this.GetUserRoleClaims(id, name));
        return claims;
    }

    private IEnumerable<Claim> GetUserRoleClaims(string id, string name)
    {
        List<Claim> claims = new List<Claim>();

        claims.Add(new Claim(ClaimTypes.NameIdentifier, id));
        claims.Add(new Claim(ClaimTypes.Role, name));
        return claims;
    }
    }
}
